var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({extended: false})

app.use(express.static('public'));
app.set('view engine', 'ejs');
app.set('views', './views');


var pg = require('pg');
var config = {
    user: 'postgres', //env var: PGUSER
    database: 'ReactJS', //env var: PGDATABASE
    password: '12345', //env var: PGPASSWORD
    host: 'localhost', // Server hosting the postgres database
    port: 5432, //env var: PGPORT
    max: 10, // max number of clients in the pool
    idleTimeoutMillis: 30000, // how long a client is allowed to remain idle before being closed
};
var pool = new pg.Pool(config);

var port = 9000;

app.get('/', function(req, res){
    pool.connect(function(err, client, done) {
        if (err) {
            return console.error('error fetching client from pool', err);
        }
        client.query('SELECT * FROM project', function(err, result) {
            done();
            if (err) {
                res.end();
                return console.error('error running query', err);
            }
			res.render('homepage', {
                project: result
            });
        });
    });
});

app.get('/projectdetail', function(req, res){
    pool.connect(function(err, client, done) {
        if (err) {
            return console.error('error fetching client from pool', err);
        }
        client.query('SELECT mem.name_mem FROM assignment ass, project pro, member mem WHERE ass.id_proj=pro.id_proj AND mem.id_mem=ass.id_mem AND pro.id_proj=1', function(err, result) {
            done();
            if (err) {
                res.end();
                return console.error('error running query', err);
            }
            res.render('showdetail', {
                list_mem: result
            });
        });
    });
});
app.get('/createproject', function(req, res){
    res.render('create-project');
});

app.post('/createproject', urlencodedParser, function(req, res){
    var name = req.body.txtNameProj;
    pool.connect(function(err, client, done) {
        if (err) {
            return console.error('error fetching client from pool', err);
        }
        client.query("INSERT INTO project (name_proj) VALUES ('"+name+"')", function(err, result) {
            done();
            if (err) {
                res.end();
                return console.error('error running query', err);
            }
            console.log('Gia tri vua nhap: ' + name);
            res.redirect('/');
        });
    });
});

app.get('/createmember', function(req, res){
    res.render('create-member');
});

app.post('/createmember', urlencodedParser, function(req, res){
    var name = req.body.txtName;
    var phone = req.body.txtPhone;
    pool.connect(function(err, client, done) {
        if (err) {
            return console.error('error fetching client from pool', err);
        }
        client.query("INSERT INTO member (name_mem, phone) VALUES ('"+name+"', '"+phone+"')", function(err, result) {
            done();
            if (err) {
                res.end();
                return console.error('error running query', err);
            }
            console.log('Gia tri vua nhap: ' + name + ' ' + phone);
            res.redirect('/');
        });
    });
});

app.listen(port, function(){
    console.log('Server started on port' + port);
});