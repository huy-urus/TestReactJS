class CreateMember extends React.Component{
    render(){
        return(
            <div>
            <h1> Create Member </h1>
            <form action='' method='POST' enctype='application/x-www-form-urlencoded'>
                <input type='text' name='txtName' placeholder='Name..' /> <br/>
                <input type='text' name='txtPhone' placeholder='Phone Number..' /> <br/>
                <input type='submit' name='btnCreate' value='Create' /> <br/>
            </form>
            </div>
        )
    }
}

ReactDOM.render(
    <div>
        <CreateMember/>
    </div>
, document.getElementById("create-member"));