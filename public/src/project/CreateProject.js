class CreateProject extends React.Component{
    render(){
        return(
            <div>
                <h1>Create Project</h1>
                <form action='' method='POST' enctype='application/x-www-form-urlencoded'>
                    <input type='text' name='txtNameProj' placeholder='Name Project..' /> <br/>
                    <input type='submit' name='btnCreateProj' value='Create' /> <br/>
                </form>
            </div>
        )
    }
}

ReactDOM.render(
    <div>
        <CreateProject/>
    </div>
, document.getElementById("create-project"));