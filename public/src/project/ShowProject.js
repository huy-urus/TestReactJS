class ShowProject extends React.Component{
    render(){
        return(
            <div>
            <h1> Project List</h1>
            <ul>
                <li><a href='/projectdetail'>Name Project-1</a></li>
                <li><a href='/projectdetail'>Name Project-2</a></li>
                <li><a href='/projectdetail'>Name Project-n</a></li>
            </ul>
            <a href='/createproject'><input type='button' value='Create Project'/></a><br/>
            <a href='/createmember'><input type='button' value='Create Member'/></a><br/>   
            </div>
        )
    }
}

ReactDOM.render(
    <div>
        <ShowProject/>
    </div>
, document.getElementById("show-project"));

