class ShowDetail extends React.Component{
    render(){
        return(
            <div>
                <h1> Name Project </h1>
                <ul>
                    <li> Name-Member-1</li>
                    <li> Name-Member-2</li>
                    <li> Name-Member-n</li>
                </ul>
            </div>
        )
    }
}

ReactDOM.render(
    <div>
        <ShowDetail/>
    </div>
, document.getElementById("show-detail"));